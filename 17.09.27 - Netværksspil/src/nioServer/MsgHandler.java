package nioServer;

import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;

/**
 * Events, from read, are handed directly here - by calling on the instance.
 * Events are stashed in a local queue and notify the thread that there's work
 * available. The worker sits in a loop waiting for events. Events == data
 * packets This echoes the data back to the sender
 *
 * @author toke
 *
 */
public class MsgHandler implements Runnable {
	private List queue = new LinkedList();

	public void processData(Server server2, SocketChannel socket, byte[] data, int count) {
		byte[] dataCopy = new byte[count];
		System.arraycopy(data, 0, dataCopy, 0, count);
		synchronized (queue) {
			queue.add(new ServerDataEvent(server2, socket, dataCopy));
			queue.notifyAll();
		}
	}

	@Override
	public void run() {
		ServerDataEvent dataEvent = null;

		while (true) {
			// Wait for data become available
			synchronized (queue) {
				while (queue.isEmpty()) {
					try {
						queue.wait();
					} catch (InterruptedException e) {

					}
				}
				dataEvent = (ServerDataEvent) queue.remove(0);
			}
			String s = new String(dataEvent.data);
			if (s.charAt(0) == 'n') {
				String[] split = s.split("#");
				dataEvent.server.createPlayer(dataEvent.socket, split[1]);
			} else if (s.charAt(0) == 'm') {
				String[] split = s.split("#");
				int x = Integer.parseInt(split[1]);
				int y = Integer.parseInt(split[2]);
				Direction dir = Direction.valueOf(split[3]);
				String name = split[4];
				dataEvent.server.movePlayer(dataEvent.socket, x, y, dir, name);
			} else if (s.charAt(0) == 's') {
				String[] split = s.split("#");
				String name = split[1];
				dataEvent.server.shoot(dataEvent.socket, name);
			} else if (s.charAt(0) == 'u') {
				dataEvent.server.getScorelist(dataEvent.socket);
			} else if (s.charAt(0) == 'r') {
				String[] split = s.split("#");
				dataEvent.server.respawnPlayer(dataEvent.socket, split[1]);
			} else if (s.charAt(0) == 'b') {
				String[] split = s.split("#");
				dataEvent.server.boardReq(dataEvent.socket);
			} else if (s.charAt(0) == 'd') {
				String[] split = s.split("#");
				String name = split[1];
				dataEvent.server.removeDC(name);
			}
		}
	}
}

package nioServer;

/**
 * Container for a data-event on the server - data-packets
 */
import java.nio.channels.SocketChannel;

public class ServerDataEvent {
	public Server server;
	public SocketChannel socket;
	public byte[] data;

	public ServerDataEvent(Server server2, SocketChannel socket, byte[] data) {
		this.server = server2;
		this.socket = socket;
		this.data = data;
	}
}

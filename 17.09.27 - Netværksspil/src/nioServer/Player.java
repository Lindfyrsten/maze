package nioServer;

import java.nio.channels.SocketChannel;

public class Player {

	private String name;
	private int xpos, ypos, point;
	private Direction direction;
	private boolean shooting = false;
	private boolean moving = false;

	private SocketChannel clientSocket;

	public Player(String name, int xpos, int ypos, Direction direction, SocketChannel socket) {
		this.name = name;
		this.xpos = xpos;
		this.ypos = ypos;
		this.direction = direction;
		this.clientSocket = socket;
		point = 0;
	}

	public int getXpos() {
		return xpos;
	}

	public void setXpos(int xpos) {
		this.xpos = xpos;
	}

	public int getYpos() {
		return ypos;
	}

	public void setYpos(int ypos) {
		this.ypos = ypos;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public void addPoints(int p) {
		point += p;
	}

	public int getPoints() {
		return point;
	}

	public String getName() {
		return name;
	}

	public boolean isShooting() {
		return shooting;
	}

	public void setShooting(boolean shooting) {
		this.shooting = shooting;
	}

	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public SocketChannel getSocketChannel() {
		return clientSocket;
	}

	@Override
	public String toString() {
		return name + ":   " + point;
	}
}

package nioServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Server implements Runnable {

	// the host:port combination to listen on
	private InetAddress hostAddress;
	private int port;

	// The channel on which we'll accept connections
	private ServerSocketChannel serverChannel;

	// The selector we'll be monitoring
	private Selector selector;

	// The buffer into which we'll read data when it's available
	private ByteBuffer readBuffer = ByteBuffer.allocate(8192);

	private static MsgHandler worker;

	// A list of ChangeRequest instances
	private List changeRequests = new LinkedList();

	// Maps a SocketChannel to a list of ByteBuffer instances
	private Map pendingData = new HashMap();

	// private List<SocketChannel> clients = new ArrayList<>();

	public Server(InetAddress hostAddress, int port, MsgHandler worker2) throws IOException {
		this.hostAddress = hostAddress;
		this.port = port;
		this.selector = this.initSelector();
		this.worker = worker2;
	}

	public static void main(String[] args) {
		try {
			worker = new MsgHandler();
			new Thread(worker).start();
			new Thread(new Server(null, 9090, worker)).start();
			mapSize = 20;
			board = Board.genBoard(mapSize, 0.4);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create and initialize a non-blocking server channel and a selector Then
	 * register the server channel with that selector
	 */
	private Selector initSelector() throws IOException {
		Selector socketSelector = SelectorProvider.provider().openSelector();

		this.serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);

		InetSocketAddress isa = new InetSocketAddress(this.hostAddress, this.port);
		serverChannel.socket().bind(isa);

		serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);

		return socketSelector;
	}

	/**
	 * Accepting connections Select loop
	 */
	@Override
	public void run() {
		while (true) {
			try {
				// Process any pending changes
				synchronized (this.changeRequests) {
					Iterator changes = this.changeRequests.iterator();
					while (changes.hasNext()) {
						ChangeRequest change = (ChangeRequest) changes.next();
						switch (change.type) {
						case ChangeRequest.CHANGEOPS:
							SelectionKey key = change.socket.keyFor(this.selector);
							if (key != null) {
								key.interestOps(change.ops);
							}
						}
					}
					this.changeRequests.clear();
				}
				// Wait for an event on one of the registered channels
				this.selector.select();

				// Iterate over the set of keys for which events are available
				Iterator selectedKeys = this.selector.selectedKeys().iterator();
				while (selectedKeys.hasNext()) {
					SelectionKey key = (SelectionKey) selectedKeys.next();
					selectedKeys.remove();

					if (!key.isValid()) {
						continue;
					}

					// Check what event is available and deal with it
					if (key.isAcceptable()) {
						this.accept(key);
					} else if (key.isReadable()) {
						this.read(key);
					} else if (key.isWritable()) {
						this.write(key);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Accept connection
	 */
	private void accept(SelectionKey key) throws IOException {
		// For an accept to be pending the channel must be a server channel
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

		// Accept the connection and make it non-blocking
		SocketChannel socketChannel = serverSocketChannel.accept();
		// clients.add(socketChannel);
		Socket socket = socketChannel.socket();
		socketChannel.configureBlocking(false);

		// Register the new SocketChannel with the selector, indicating
		// we'd like to be notifed when there's data waiting to be read
		socketChannel.register(this.selector, SelectionKey.OP_READ);
	}

	/**
	 * Read data from an accepted connection
	 */
	private void read(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		// Clear out the read buffer so it's ready for new data
		this.readBuffer.clear();

		// Attempt to read off the channel
		int numRead;
		try {
			numRead = socketChannel.read(this.readBuffer);
		} catch (IOException e) {
			// The remote forcibly closed the connection, cancel
			// the selection key and close the channel
			key.cancel();
			socketChannel.close();
			return;
		}
		if (numRead == -1) {
			// Remote entity shut the socket down cleanly. Do the
			// same from our end and cancel the channel
			key.channel().close();
			key.cancel();
			return;
		}

		// Hand the data off to our worker thread
		this.worker.processData(this, socketChannel, this.readBuffer.array(), numRead);
	}

	/**
	 * Writing data to a channel We need to know that the channel is ready for
	 * more data - which means setting the OP_WRITE interest op flag on that
	 * channel's selection key.
	 */
	public void send(SocketChannel socket, byte[] data) {
		synchronized (this.changeRequests) {
			// Indicate we want the interest ops set changed
			this.changeRequests.add(new ChangeRequest(socket, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

			// And queue the data we want written
			synchronized (this.pendingData) {
				List queue = (List) this.pendingData.get(socket);
				if (queue == null) {
					queue = new ArrayList();
					this.pendingData.put(socket, queue);
				}
				queue.add(ByteBuffer.wrap(data));
			}
		}

		// Finally wake up the selecting thread so it can make the required
		// changes - relies on additional code in run
		this.selector.wakeup();
	}

	/**
	 * Needs to pull data off the appropriate queue and keep writing it until it
	 * runs out or can't write anymore. ByteBuffers added to per-socket internal
	 * queue - its a convenient way to track how much data remains in the
	 * buffer. Using a ByteBuffer saves us having to either resize byte arrays
	 * in the queue or track an index into the byte array at the head of the
	 * queue.
	 */
	private void write(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		synchronized (this.pendingData) {
			List queue = (List) this.pendingData.get(socketChannel);

			// Write until there is no more data ...
			while (!queue.isEmpty()) {
				checkPlayerList();
				ByteBuffer buf = (ByteBuffer) queue.get(0);
				socketChannel.write(buf);
				if (buf.remaining() > 0) {
					// ... or the socket's buffer fills up
					break;
				}
				queue.remove(0);
			}

			if (queue.isEmpty()) {
				// All the data has been written away
				// Set socket to wait for data
				key.interestOps(SelectionKey.OP_READ);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private static List<Player> players = new ArrayList<>();

	private static int mapSize;

	private static String[] board;

	public void createPlayer(SocketChannel socket, String name) {

		Player p = new Player(name, 0, 0, Direction.UP, socket);
		spawnPlayer(p);
		players.add(p);
		byte[] cData = ("c#" + p.getXpos() + "#" + p.getYpos() + "#" + p.getName() + "#" + '\n').getBytes();

		for (Player pl : players) {
			this.send(pl.getSocketChannel(), cData);
		}

	}

	private void spawnPlayer(Player player) {
		boolean respawned = false;
		int x = 0;
		int y = 0;
		while (!respawned) {
			x = (int) (Math.random() * (board.length - 2) + 1);
			y = (int) (Math.random() * (board.length - 2) + 1);
			if (board[y].charAt(x) != 'w' && getPlayerAt(x, y) == null) {
				player.setXpos(x);
				player.setYpos(y);
				respawned = true;
			}
		}
	}

	private Player getPlayerAt(int x, int y) {
		boolean found = false;
		int i = 0;

		Player player = null;

		while (!found && i < players.size()) {
			player = players.get(i);
			if (player.getXpos() == x && player.getYpos() == y) {
				found = true;
			} else {
				i++;
			}
		}
		if (found) {
			return player;
		} else {
			return null;
		}
	}

	private static Player findPlayer(String name) {
		boolean found = false;
		int i = 0;
		Player player = null;

		while (!found && i < players.size()) {
			player = players.get(i);
			String foundName = player.getName();
			if (name.equals(foundName)) {
				found = true;
			} else {
				i++;
			}
		}
		if (found) {
			return player;
		} else {
			return null;
		}
	}

	public void movePlayer(SocketChannel socket, int toX, int toY, Direction dir, String name) {
		Player p = findPlayer(name);
		if (p != null) {

			int fromX = p.getXpos();
			int fromY = p.getYpos();
			int newX = fromX + toX;
			int newY = fromY + toY;

			Direction newDir;
			if (toX == 0 && toY == -1) {
				newDir = Direction.UP;
			} else if (toX == 0 && toY == 1) {
				newDir = Direction.DOWN;
			} else if (toX == -1 && toY == 0) {
				newDir = Direction.LEFT;
			} else {
				newDir = Direction.RIGHT;
			}

			if (newX >= 0 && newX < board[0].length() && newY >= 0 && newY < board[1].length()) {
				if (board[newY].charAt(newX) != 'w' && getPlayerAt(newX, newY) == null) {
					p.setXpos(newX);
					p.setYpos(newY);
				} else {
					newX = fromX;
					newY = fromY;
				}
			} else {
				newX = fromX;
				newY = fromY;
			}
			p.setDirection(newDir);
			byte[] cData = ("m#" + fromX + "#" + fromY + "#" + newX + "#" + newY + "#" + newDir.toString() + "#" + name
					+ "#" + '\n').getBytes();

			for (Player pl : players) {
				this.send(pl.getSocketChannel(), cData);
			}
		}
	}

	public void shoot(SocketChannel socket, String name) {
		Player shooter = findPlayer(name);
		Player playerShot = null;

		if (shooter != null) {

			int delta_x, delta_y;
			int x = shooter.getXpos();
			int y = shooter.getYpos();
			Direction direction = shooter.getDirection();
			if (direction == Direction.UP) {
				delta_x = 0;
				delta_y = -1;
			} else if (direction == Direction.DOWN) {
				delta_x = 0;
				delta_y = 1;
			} else if (direction == Direction.LEFT) {
				delta_x = -1;
				delta_y = 0;
			} else {
				delta_x = 1;
				delta_y = 0;
			}
			int startX = x + delta_x;
			int startY = y + delta_y;
			byte[] cData = (new String("\n")).getBytes();

			if (startX >= 0 && startX < board[0].length() && startY >= 0 && startY < board[1].length()) {
				if (board[startY].charAt(startX) != 'w') {
					boolean chk = true;
					while (chk) {
						if ((x + delta_x) >= 0 && (x + delta_x) < board[0].length() && (y + delta_y) >= 0
								&& (y + delta_y) < board[1].length()) {
							if (board[y + delta_y].charAt(x + delta_x) != 'w') {
								x += delta_x;
								y += delta_y;
								checkPlayerList();
								for (Player p : players) {
									if (p != shooter) {
										if (p.getXpos() == x && p.getYpos() == y) {
											playerShot = p;
											shooter.addPoints(50);
											playerShot.addPoints(-50);
										}
									}
								}
							} else {
								chk = false;
							}
						} else {
							chk = false;
						}

					}
				}
			}
			if (x != shooter.getXpos() || y != shooter.getYpos()) {
				String deadName = "";
				int deadX = -1;
				int deadY = -1;
				if (playerShot != null) {
					deadName = playerShot.getName();
					deadX = playerShot.getXpos();
					deadY = playerShot.getYpos();
				}
				String s = ("s#" + startX + "#" + startY + "#" + x + "#" + y + "#" + direction.toString() + "#" + name
						+ "#" + deadName + "#" + deadX + "#" + deadY + "#" + '\n');

				cData = s.getBytes();
			}
			for (Player pl : players) {
				this.send(pl.getSocketChannel(), cData);
			}
		}

	}

	public void respawnPlayer(SocketChannel socket, String playerName) {

		Player deadPlayer = findPlayer(playerName);

		if (deadPlayer != null) {
			spawnPlayer(deadPlayer);
			String s = "r#" + playerName + "#" + deadPlayer.getXpos() + "#" + deadPlayer.getYpos() + "#" + '\n';
			byte[] cData = s.getBytes();

			for (Player pl : players) {
				this.send(pl.getSocketChannel(), cData);
			}
		}
	}

	public void getScorelist(SocketChannel socket) {
		String s = "u";
		if (players.size() > 0) {
			for (Player p : players) {
				s += "#" + p.getName() + "#" + p.getPoints();
			}
		}
		s += "#" + '\n';
		byte[] cData = s.getBytes();

		for (Player pl : players) {
			this.send(pl.getSocketChannel(), cData);
		}
	}

	public void removeDC(String name) {
		Player dc = findPlayer(name);
		if (dc != null) {
			int x = dc.getXpos();
			int y = dc.getYpos();
			players.remove(dc);
			String s = "d#" + x + "#" + y + "#" + '\n';
			byte[] cData = s.getBytes();
			for (Player pl : players) {
				this.send(pl.getSocketChannel(), cData);
			}
		}
	}

	public void boardReq(SocketChannel socket) {
		String s = "b#" + mapSize;
		for (int i = 0; i < board.length; i++) {
			s += "#" + board[i];
		}
		s += "#" + '\n';
		byte[] cData = s.getBytes();

		this.send(socket, cData);
	}

	private void checkPlayerList() {
		synchronized (this.players) {
			for (int i = 0; i < players.size(); i++) {
				Player p = players.get(i);
				if (p != null && !p.getSocketChannel().isConnected()) {
					Player pl = p;
					removeDC(pl.getName());
				}
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

}

package nioServer.boardgen;

public class Coord {
	private int x;
	private int y;

	public Coord(int _x, int _y) {
		x = _x;
		y = _y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public boolean equals(Object other) {
		boolean result = false;
		if (other instanceof Coord) {
			Coord that = (Coord) other;
			result = (this.getX() == that.getX() && this.getY() == that.getY());
		}
		return result;
	}

	/*
	 * define ==(Coord c1, Coord c2) { return c1.x == c2.x && c1.y == c2.y; }
	 *
	 * define !=(Coord c1, Coord c2) { return !(c1 == c2); }
	 */

}

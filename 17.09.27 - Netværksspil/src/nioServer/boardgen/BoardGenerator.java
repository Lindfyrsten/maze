package nioServer.boardgen;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

public class BoardGenerator {

	private List<Coord> allTileCoords;
	private Queue<Coord> shuffledTileCoords;
	private int seed;
	private int mapSize;

	private Coord mapCentre;
	private double obstaclePercent;

	public BoardGenerator(int mapSize, double obstaclePercent, int seed) {
		this.mapSize = mapSize;
		this.obstaclePercent = obstaclePercent;
		this.seed = seed;
	}

	public void GenBoard() {
		allTileCoords = new ArrayList<Coord>();
		for (int x = 0; x < mapSize; x++) {
			for (int y = 0; y < mapSize; y++) {
				allTileCoords.add(new Coord(x, y));
			}
		}
		shuffledTileCoords = new LinkedList<Coord>(shuffleArray(allTileCoords, seed));

		for (int x = 0; x < mapSize; x++) {
			for (int y = 0; y < mapSize; y++) {
				// instantiate floor tiles at position
			}
		}

		mapCentre = new Coord(mapSize / 2, mapSize / 2);

		boolean[][] obstacleMap = new boolean[mapSize][mapSize];
		int obstacleCount = (int) ((mapSize * mapSize) * obstaclePercent);
		int currentObstacleCount = 0;

		// for (int x = 0; x < obstacleMap[0].length; x++) {
		// for (int y = 0; y < obstacleMap[1].length; y++) {
		// if (x == 0) {
		// obstacleMap[x][y] = true;
		// currentObstacleCount++;
		// } else if (x == obstacleMap[0].length - 1) {
		// obstacleMap[x][y] = true;
		// currentObstacleCount++;
		//
		// } else if (y == 0) {
		// obstacleMap[x][y] = true;
		// currentObstacleCount++;
		//
		// } else if (y == obstacleMap[1].length - 1) {
		// obstacleMap[x][y] = true;
		// currentObstacleCount++;
		//
		// }
		// }
		// }

		for (int i = 0; i < obstacleCount; i++) {
			Coord randomCoord = getRandomCoord();
			obstacleMap[randomCoord.getX()][randomCoord.getY()] = true;
			currentObstacleCount++;

			if (!randomCoord.equals(mapCentre) && MapFullyAccessible(obstacleMap, currentObstacleCount)) {
				// instantiate obstacle @ randomCoord
			} else {
				obstacleMap[randomCoord.getX()][randomCoord.getY()] = false;
				currentObstacleCount--;
			}
		}
		String[] board = new String[mapSize];
		int index = 0;
		for (int x = 0; x < obstacleMap[0].length; x++) {
			System.out.println();
			String line = "";
			for (int y = 0; y < obstacleMap[1].length; y++) {
				if (obstacleMap[x][y]) {
					System.out.print("w");
					line += "w";
				} else {
					System.out.print("o");
					line += "o";
				}
			}
			board[index] = line;
			index++;
		}
		System.out.println();

		for (String s : board) {
			System.out.println(s);
		}
	}

	private Coord getRandomCoord() {
		Coord randomCoord = shuffledTileCoords.remove();
		shuffledTileCoords.add(randomCoord);
		return randomCoord;
	}

	private boolean MapFullyAccessible(boolean[][] obstacleMap, int currentObstacleCount) {
		boolean[][] mapflags = new boolean[obstacleMap[0].length][obstacleMap[1].length];
		Queue<Coord> queue = new LinkedList<Coord>();
		queue.add(mapCentre);
		mapflags[mapCentre.getX()][mapCentre.getY()] = true;

		int accessibleTileCount = 1;

		while (queue.size() > 0) {
			Coord tile = queue.remove();

			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					int neighbourX = tile.getX() + x;
					int neighbourY = tile.getY() + y;
					if (x == 0 || y == 0) {
						if (neighbourX >= 0 && neighbourX < obstacleMap[0].length && neighbourY >= 0
								&& neighbourY < obstacleMap[1].length) {
							if (!mapflags[neighbourX][neighbourY] && !obstacleMap[neighbourX][neighbourY]) {
								mapflags[neighbourX][neighbourY] = true;
								queue.add(new Coord(neighbourX, neighbourY));
								accessibleTileCount++;
							}
						}
					}
				}
			}
		}
		int targetAccessibleTileCount = (mapSize * mapSize) - currentObstacleCount;
		return targetAccessibleTileCount == accessibleTileCount;
	}

	private List<Coord> shuffleArray(List<Coord> array, int seed) {
		for (int i = 0; i < array.size() - 1; i++) {
			int randomIndex = generateRandom(i, array.size());
			Coord temp = array.get(randomIndex);
			array.set(randomIndex, array.get(i));
			array.set(i, temp);
		}
		return array;
	}

	private int generateRandom(int min, int max) {
		Random r = new Random();
		return r.nextInt(max - min) + min;
	}

}

package nioClient;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

	private static Stage stage;

	public static final int size = 30;
	public static final int scene_height = size * 20 + 100;
	public static final int scene_width = size * 20 + 200;

	public static Image image_floor1, blood;
	public static Image image_wall1, image_wall2, image_wall3, image_wall4, image_wall5;
	public static Image hero_right, hero_left, hero_up, hero_down, hero_dead, hero_dead2;
	public static Image fireDown, fireHorizontal, fireLeft, fireRight, fireUp, fireVertical, fireWallEast,
			fireWallNorth, fireWallSouth, fireWallWest;
	public static Image hero_up2;
	public static Image hero_rightRed, hero_leftRed, hero_upRed, hero_downRed;
	public static ArrayList<Image> walls = new ArrayList<>();

	private static String name;
	private static boolean shooting;
	private static boolean dead;

	private static int mapSize;
	private static Label[][] fields;
	private static TextArea scoreList;

	private static String[] board;

	private static Client ioHandler;

	public static void main(String[] args) {

		launch(args);
	}

	private void launchClient(InetAddress host) {
		RspHandler handler = new RspHandler();
		try {
			Thread t1 = new Thread(handler);
			t1.setDaemon(true);
			t1.start();
			ioHandler = new Client(host, 9090, handler);
			Thread t2 = new Thread(ioHandler);
			t2.setDaemon(true);
			t2.start();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;

		// load images
		loadImages();
		mainMenu();
	}

	private Scene mainMenu() {
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(40, 10, 0, 10));

		BackgroundImage tableImage = new BackgroundImage(
				new Image(getClass().getResourceAsStream("image/table.jpg/"), scene_width, scene_height, false, true),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		Background table = new Background(tableImage);
		grid.setBackground(table);

		Button createServerBtn = new Button("Host Server");
		createServerBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
			}
		});
		createServerBtn.setAlignment(Pos.CENTER);
		grid.add(createServerBtn, 0, 0);

		Button joinServerBtn = new Button("Join Server");
		joinServerBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				InetAddress hostToJoin = null;
				String s = null;
				TextInputDialog dialog = new TextInputDialog();
				dialog.setHeaderText("What's your name?");
				dialog.setContentText("Please enter the host address:");
				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()) {
					s = result.get();

				}
				try {
					hostToJoin = InetAddress.getByName(s);
				} catch (UnknownHostException e2) {
					// e2.printStackTrace();
				}
				launchClient(hostToJoin);

				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Joining Game");
				alert.setHeaderText(null);
				alert.setContentText("Joining host at " + s);
				getBoardReq();

				alert.showAndWait();

				TextInputDialog dialogName = new TextInputDialog();
				dialogName.setHeaderText("What's your name?");
				dialogName.setContentText("Please enter your name:");
				Optional<String> dName = dialogName.showAndWait();
				if (dName.isPresent()) {
					name = dName.get();
				}

				generateLevelScn(board);
			}
		});
		grid.add(joinServerBtn, 0, 1);

		// create and load scene
		Scene scene = new Scene(grid, scene_width, scene_height);

		stage.setScene(scene);
		stage.show();
		return scene;
	}

	private Scene generateLevelScn(String[] board) {
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(40, 10, 0, 10));

		Text scoreLabel = new Text("Score");
		scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 36));
		scoreLabel.setFill(Color.DARKRED);
		scoreList = new TextArea();
		scoreList.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
		scoreList.setEditable(false);
		scoreList.setMaxHeight(200);
		GridPane boardGrid = new GridPane();
		boardGrid.setBorder(
				new Border(new BorderStroke(Color.DARKGRAY, BorderStrokeStyle.SOLID, null, new BorderWidths(10))));
		Collections.addAll(walls, image_wall3, image_wall4, image_wall5);

		BackgroundImage tableImage = new BackgroundImage(
				new Image(getClass().getResourceAsStream("image/table.jpg/"), scene_width, scene_height, false, true),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		Background table = new Background(tableImage);
		grid.setBackground(table);

		while (fields != null) {
		}
		fields = new Label[mapSize][mapSize];
		for (int j = 0; j < fields[0].length; j++) {
			for (int i = 0; i < fields[1].length; i++) {
				switch (board[j].charAt(i)) {
				case 'w':
					int random = (int) (Math.random() * walls.size());
					ImageView img = new ImageView(walls.get(random));
					img.setOpacity(.9);
					fields[i][j] = new Label("", img);
					break;
				case ' ':
					fields[i][j] = new Label("", null);
					break;
				default:
					try {
						throw new Exception("Illegal field value: " + board[j].charAt(i));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				boardGrid.add(fields[i][j], i, j);
			}
		}
		grid.add(boardGrid, 0, 1, 1, 2);
		grid.add(scoreLabel, 1, 1);
		grid.add(scoreList, 1, 2);
		GridPane.setHalignment(scoreLabel, HPos.CENTER);
		GridPane.setValignment(scoreList, VPos.TOP);

		// create and load scene
		Scene scene = new Scene(grid, scene_width, scene_height);

		// set key events
		scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			switch (event.getCode()) {
			case UP:
				try {
					moveReq(0, -1, Direction.UP);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case DOWN:
				try {
					moveReq(0, +1, Direction.DOWN);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case LEFT:
				try {
					moveReq(-1, 0, Direction.LEFT);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case RIGHT:
				try {
					moveReq(+1, 0, Direction.RIGHT);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case SPACE:
				shootReq();
				break;
			case ESCAPE:
				try {
					quit();
				} catch (Exception e) {
					e.printStackTrace();
				}
			default:
				break;
			}
		});

		stage.setScene(scene);
		stage.show();

		createPlayerReq(name);
		return scene;
	}

	public void loadImages() {
		image_wall1 = new Image(getClass().getResourceAsStream("image/wall1.png"), size, size, false, false);
		image_wall2 = new Image(getClass().getResourceAsStream("image/wall2.png"), size, size, false, false);
		image_wall3 = new Image(getClass().getResourceAsStream("image/wall3.png"), size, size, false, false);
		image_wall4 = new Image(getClass().getResourceAsStream("image/wall4.png"), size, size, false, false);
		image_wall5 = new Image(getClass().getResourceAsStream("image/wall5.png"), size, size, false, false);
		hero_right = new Image(getClass().getResourceAsStream("image/heroRight.png"), size, size, false, false);
		hero_left = new Image(getClass().getResourceAsStream("image/heroLeft.png"), size, size, false, false);
		hero_up = new Image(getClass().getResourceAsStream("image/heroUp.png"), size, size, false, false);
		hero_down = new Image(getClass().getResourceAsStream("image/heroDown.png"), size, size, false, false);
		hero_rightRed = new Image(getClass().getResourceAsStream("image/heroRightRed.png"), size, size, false, false);
		hero_leftRed = new Image(getClass().getResourceAsStream("image/heroLeftRed.png"), size, size, false, false);
		hero_upRed = new Image(getClass().getResourceAsStream("image/heroUpRed.png"), size, size, false, false);
		hero_downRed = new Image(getClass().getResourceAsStream("image/heroDownRed.png"), size, size, false, false);
		image_floor1 = new Image(getClass().getResourceAsStream("image/floor1.png"), size, size, false, false);
		fireDown = new Image(getClass().getResourceAsStream("image/fireDown.png"), size, size, false, false);
		fireUp = new Image(getClass().getResourceAsStream("image/fireUp.png"), size, size, false, false);
		fireLeft = new Image(getClass().getResourceAsStream("image/fireLeft.png"), size, size, false, false);
		fireRight = new Image(getClass().getResourceAsStream("image/fireRight.png"), size, size, false, false);
		fireHorizontal = new Image(getClass().getResourceAsStream("image/fireHorizontal.png"), size, size, false,
				false);
		fireVertical = new Image(getClass().getResourceAsStream("image/fireVertical.png"), size, size, false, false);
		fireWallEast = new Image(getClass().getResourceAsStream("image/fireWallEast.png"), size, size, false, false);
		fireWallNorth = new Image(getClass().getResourceAsStream("image/fireWallNorth.png"), size, size, false, false);
		fireWallWest = new Image(getClass().getResourceAsStream("image/fireWallWest.png"), size, size, false, false);
		fireWallSouth = new Image(getClass().getResourceAsStream("image/fireWallSouth.png"), size, size, false, false);
		hero_dead = new Image(getClass().getResourceAsStream("image/heroDead.png"), size, size, false, false);
		hero_dead2 = new Image(getClass().getResourceAsStream("image/heroDead2.png"), size, size, false, false);
		blood = new Image(getClass().getResourceAsStream("image/blood.png"), size, size, false, false);
		hero_up2 = new Image(getClass().getResourceAsStream("image/heroUp2.png"), size, size, false, false);
	}

	/**
	 * The client moves thier own character
	 *
	 * @param delta_x
	 * @param delta_y
	 * @param direction
	 */
	private void moveReq(int delta_x, int delta_y, Direction direction) {
		if (!dead && !shooting) {
			String s = "m#" + delta_x + "#" + delta_y + "#" + direction.toString() + "#" + name + "#" + '\n';
			try {
				ioHandler.send(s.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Displays a character move from any client
	 *
	 * @param fromX
	 * @param fromY
	 * @param toX
	 * @param toY
	 * @param direction
	 */
	public static void movePlayer(int fromX, int fromY, int toX, int toY, Direction direction, String playerName) {
		fields[fromX][fromY].setGraphic(null);
		if (direction == Direction.RIGHT) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_right));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_rightRed));
			}
		}
		if (direction == Direction.LEFT) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_left));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_leftRed));
			}
		}
		if (direction == Direction.UP) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_up));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_upRed));
			}
		}
		if (direction == Direction.DOWN) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_down));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_downRed));
			}
		}
	}

	private void createPlayerReq(String name) {
		String s = ("n#" + name + "#" + '\n');
		try {
			ioHandler.send(s.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void spawnPlayer(int x, int y, String playerName) {
		if (playerName.equals(name)) {
			dead = false;
			shooting = false;
			fields[x][y].setGraphic(new ImageView(hero_up));
		} else {
			fields[x][y].setGraphic(new ImageView(hero_upRed));
		}
		scoreReq();

	}

	private void shootReq() {
		if (!dead) {
			if (!shooting) {
				String s = "s#" + name + "#" + '\n';
				try {
					ioHandler.send(s.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static int x, y;

	public static void playerShot(int fromX, int fromY, int toX, int toY, Direction direction, String playerName,
			String deadName, int deadX, int deadY) {
		String incName = playerName;
		if (incName.equals(name)) {
			shooting = true;
		}
		x = fromX;
		y = fromY;
		Image wall;
		Image fire;
		Image firePath;
		int delta_x, delta_y;
		if (direction == Direction.UP) {
			wall = fireWallNorth;
			fire = fireUp;
			firePath = fireVertical;
			delta_x = 0;
			delta_y = -1;
		} else if (direction == Direction.DOWN) {
			wall = fireWallSouth;
			fire = fireDown;
			firePath = fireVertical;
			delta_x = 0;
			delta_y = 1;
		} else if (direction == Direction.LEFT) {
			wall = fireWallWest;
			fire = fireLeft;
			firePath = fireHorizontal;
			delta_x = -1;
			delta_y = 0;
		} else {
			wall = fireWallEast;
			fire = fireRight;
			firePath = fireHorizontal;
			delta_x = 1;
			delta_y = 0;
		}
		int startX = x;
		int startY = y;
		while (x != toX || y != toY) {
			if (x == startX && y == startY) {
				fields[x][y].setGraphic(new ImageView(fire));
			} else {
				fields[x][y].setGraphic(new ImageView(firePath));
			}
			x += delta_x;
			y += delta_y;
		}
		x = toX;
		y = toY;
		fields[x][y].setGraphic(new ImageView(wall));

		Task task = new Task() {
			int fX = fromX;
			int fY = fromY;
			int cX = x;
			int cY = y;
			int dX = delta_x;
			int dY = delta_y;
			int dedX = deadX;
			int dedY = deadY;

			@Override
			protected Object call() throws Exception {
				Timeline shoot = new Timeline(new KeyFrame(Duration.seconds(0.2), e -> {
					fields[cX][cY].setGraphic(null);
					while (cX != fromX || cY != fromY) {
						cX -= dX;
						cY -= dY;
						fields[cX][cY].setGraphic(null);
					}
					if (dedX != -1 && dedY != -1) {
						fields[dedX][dedY].setGraphic(new ImageView(blood));
					}
				}));
				shoot.play();
				shoot.setOnFinished(e -> {
					shooting = false;
				});
				return null;
			}

		};
		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();

		// Timeline shoot = new Timeline(new KeyFrame(Duration.seconds(.2), e ->
		// {
		// fields[x][y].setGraphic(null);
		// while (x != fromX || y != fromY) {
		// x -= delta_x;
		// y -= delta_y;
		// fields[x][y].setGraphic(null);
		// }
		// if (deadX != -1 && deadY != -1) {
		// fields[deadX][deadY].setGraphic(new ImageView(blood));
		// }
		// }));
		// shoot.play();
		// shoot.setOnFinished(e -> {
		// shooting = false;
		// });
		if (deadName.equals(name)) {
			dead = true;
			respawnReq();
		}
	}

	private static void respawnReq() {
		if (dead) {
			String s = "r#" + name + "#" + '\n';
			try {
				ioHandler.send(s.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void scoreReq() {
		String s = "u#" + '\n';
		try {
			ioHandler.send(s.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void updateScore(String[] scores) {
		StringBuffer b = new StringBuffer(100);
		for (int i = 1; i < scores.length - 1; i++) {
			if (scores[i] != "\n" && scores[i] != null && scores[i + 1] != "\n" && scores[i + 1] != null) {
				b.append(scores[i] + ": " + scores[i + 1] + "\n");
				i++;
			}
		}
		scoreList.clear();
		scoreList.setText(b.toString());
	}

	public static void removeDC(int x, int y) {
		fields[x][y].setGraphic(null);
	}

	private static void sendDC() {
		String s = "d#" + name + "#" + '\n';
		try {
			System.out.println("DC sent");
			ioHandler.send(s.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void getBoardReq() {
		String s = "b#" + '\n';
		try {
			ioHandler.send(s.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void recieveBoard(int mapSize, String[] board) {
		Main.mapSize = mapSize;
		Main.board = board;
	}

	private void quit() {
		sendDC();
		Platform.exit();
	}

}

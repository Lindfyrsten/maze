package nioClient;

import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;

import javafx.application.Platform;

public class RspHandler implements Runnable {
	private List queue = new LinkedList();

	public void processData(Client client, SocketChannel socket, byte[] data, int count) {
		byte[] dataCopy = new byte[count];
		System.arraycopy(data, 0, dataCopy, 0, count);
		synchronized (queue) {
			queue.add(new ClientDataEvent(client, socket, dataCopy));
			queue.notifyAll();
		}
	}

	@Override
	public void run() {
		ClientDataEvent dataEvent = null;

		while (true) {
			// Wait for data become available
			synchronized (queue) {
				while (queue.isEmpty()) {
					try {
						queue.wait();
					} catch (InterruptedException e) {

					}
				}
				dataEvent = (ClientDataEvent) queue.remove(0);
			}
			String s = new String(dataEvent.data);
			if (s.charAt(0) == 'c') {
				String[] split = s.split("#");
				int x = Integer.parseInt(split[1]);
				int y = Integer.parseInt(split[2]);
				String name = split[3];
				Platform.runLater(() -> {
					Main.spawnPlayer(x, y, name);
				});
			} else if (s.charAt(0) == 'm') {
				String[] split = s.split("#");
				int fromX = Integer.parseInt(split[1]);
				int fromY = Integer.parseInt(split[2]);
				int toX = Integer.parseInt(split[3]);
				int toY = Integer.parseInt(split[4]);
				Direction dir = Direction.valueOf(split[5]);
				String name = split[6];
				Platform.runLater(() -> {
					Main.movePlayer(fromX, fromY, toX, toY, dir, name);
				});
			} else if (s.charAt(0) == 's') {
				String[] split = s.split("#");
				int fromX = Integer.parseInt(split[1]);
				int fromY = Integer.parseInt(split[2]);
				int toX = Integer.parseInt(split[3]);
				int toY = Integer.parseInt(split[4]);
				Direction dir = Direction.valueOf(split[5]);
				String name = split[6];
				String deadName = split[7];
				int deadX = Integer.parseInt(split[8]);
				int deadY = Integer.parseInt(split[9]);
				Platform.runLater(() -> {
					Main.playerShot(fromX, fromY, toX, toY, dir, name, deadName, deadX, deadY);
				});
			} else if (s.charAt(0) == 'u') {
				String[] split = s.split("#");
				Platform.runLater(() -> {
					try {
						Main.updateScore(split);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			} else if (s.charAt(0) == 'r') {
				String[] split = s.split("#");
				String name = split[1];
				int x = Integer.parseInt(split[2]);
				int y = Integer.parseInt(split[3]);
				Platform.runLater(() -> {
					Main.spawnPlayer(x, y, name);
				});
			} else if (s.charAt(0) == 'd') {
				String[] split = s.split("#");
				int x = Integer.parseInt(split[1]);
				int y = Integer.parseInt(split[2]);
				Platform.runLater(() -> {
					System.out.println("Remove player");
					Main.removeDC(x, y);
				});
			} else if (s.charAt(0) == 'b') {
				String[] split = s.split("#");
				int mapSize = Integer.parseInt(split[1]);
				String[] board = new String[mapSize];
				for (int i = 2; i < split.length - 1; i++) {
					board[i - 2] = split[i];
				}
				Platform.runLater(() -> {
					Main.recieveBoard(mapSize, board);
				});
			}
		}
	}
}

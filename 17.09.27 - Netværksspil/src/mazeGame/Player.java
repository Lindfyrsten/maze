package mazeGame;
public class Player {

	private String name;
	private int xpos, ypos, point;
	private Direction direction;
	boolean shooting = false;
	boolean moving = false;
	private int ammo;

	public Player(String name, int xpos, int ypos, Direction direction) {
		this.name = name;
		this.xpos = xpos;
		this.ypos = ypos;
		this.direction = direction;
		point = 0;
		ammo = 99;
	}

	public int getXpos() {
		return xpos;
	}

	public void setXpos(int xpos) {
		this.xpos = xpos;
	}

	public int getYpos() {
		return ypos;
	}

	public void setYpos(int ypos) {
		this.ypos = ypos;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public void addPoints(int p) {
		point += p;
	}

	public String getName() {
		return name;
	}

	public boolean isShooting() {
		return shooting;
	}

	public void setShooting(boolean shooting) {
		this.shooting = shooting;
	}

	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public int getPoint() {
		return point;
	}

	public int getAmmo() {
		return ammo;
	}

	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}

	@Override
	public String toString() {
		return name + ":   " + point;
	}
}

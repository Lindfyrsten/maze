package mazeGame;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

    public static ArrayList<ServerThread> threads;
    public static String[] board = { // 20x20
        "wwwwwwwwwwwwwwwwwwww", "w        ww        w", "w w  w  www w  w  ww",
        "w w  w   ww w  w  ww",
        "w  w               w", "w w w w w w w  w  ww", "w w     www w  w  ww",
        "w w     w w w  w  ww",
        "w   w w     w  w   w", "w     w  w  w  w   w", "w ww ww        w  ww",
        "w  w w    w    w  ww",
        "w        ww w  w  ww", "w         w w  w  ww", "w        w     w  ww",
        "w  w              ww",
        "w  w www  w w  ww ww", "w w      ww w     ww", "w   w   ww  w      w",
        "wwwwwwwwwwwwwwwwwwww" };
    public static Player playerShot;
    public static ArrayList<Player> players = new ArrayList<>();

    public static void main(String[] args) throws Exception {

        threads = new ArrayList<>();
        // accept clients
        System.out.println("Server running...");
        ServerSocket welcomeSocket = new ServerSocket(6789);
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            System.out.println(
                "Connection received from " + connectionSocket.getInetAddress().getHostName());
            (new ServerThread(connectionSocket)).start();
        }
    }
    
    /**
     * Move a player
     *
     * @param delta_x The number on the players x value to move
     * @param delta_y The number on the players y value to move
     * @param direction The direction of the move
     * @param name The name of the player that moved
     */
    public static void movePlayer(int delta_x, int delta_y, Direction direction, String name)
        throws Exception {

        Player p = findPlayer(name);
        int fromX = p.getXpos();
        int fromY = p.getYpos();
        // check if move is legal
        if (playerMoved(delta_x, delta_y, direction, p)) {
            // tell clients to move player, if move is legal
            clientsMovePlayer(fromX, fromY, p.getXpos(), p.getYpos(), direction, name);
        }
        else {
            // if not, only update the direction player was trying to move - x,y remain the same
            clientsMovePlayer(fromX, fromY, fromX, fromY, direction, name);
        }
    }

    /**
     * Tell all server threads to update their client that a player has spawned
     */
    public static void clientsSpawnPlayers() {

        for (ServerThread t : threads) {
            try {
                for (Player p : players) {
                    t.spawnPlayer(p.getXpos(), p.getYpos(), p.getName());
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Tell all server threads to update their client that a player has moved
     */
    public static void clientsMovePlayer(int fromX, int fromY, int toX, int toY,
        Direction direction,
        String name) {

        for (ServerThread t : threads) {
            try {
                t.movePlayer(fromX, toX, fromY, toY, direction, name);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Tell all server threads to update their client that a player has shot
     */
    public static void clientsShot(int fromX, int fromY, int toX, int toY,
        Direction direction,
        String name) {

        for (ServerThread t : threads) {
            try {
                t.playerShoot(fromX, fromY, toX, toY, direction, name);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Tell all server threads to update their client's scores
     */
    public static void clientsScore() {

        String scores = "";
        for (Player p : players) {
            scores += p.getName() + "#" + p.getPoint() + "#";
        }
        for (ServerThread t : threads) {
            try {
                t.updateScores(scores);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Tell all server threads to update their client that a player has quit
     */
    public static void clientsRemovePlayer(int x, int y, String name) {

        for (ServerThread t : threads) {
            try {
                t.removePlayer(x, y, name);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // ----------------------------------------
    // Game methods
    // ----------------------------------------
    
    /**
     * Create a new player and add to player list
     *
     * @param name Name of the player
     */
    public static void createPlayer(String name) {

        Player p = new Player(name, 0, 0, Direction.UP);
        spawn(p);
        players.add(p);
    }

    /**
     * Remove a player from the player list
     *
     * @param name Name of the player
     */
    public static void removePlayer(String name) {

        // find the correct player
        Player p = findPlayer(name);
        // save the x,y coordinate before removal
        int x = p.getXpos();
        int y = p.getYpos();
        players.remove(p);
        // tell clients that a player was removed
        clientsRemovePlayer(x, y, name);
    }

    /**
     * Spawn a player at a random x,y location
     *
     * @param player Player to spawn
     */
    public static boolean spawn(Player player) {

        boolean spawned = false;
        int x = 0;
        int y = 0;
        while (!spawned) {
            // get a random x, y
            x = (int) (Math.random() * (board.length - 2) + 1);
            y = (int) (Math.random() * (board.length - 2) + 1);
            // if there's no wall && no players, set spawned to true and update the players x,y
            if (board[y].charAt(x) != 'w' && getPlayerAt(x, y) == null) {
                player.setXpos(x);
                player.setYpos(y);
                spawned = true;
            }
        }
        return spawned;
    }

    /**
     * Find a player on the player list
     *
     * @param name The name of the player
     */
    public static Player findPlayer(String name) {

        Player player = null;
        for (Player p : players) {
            if (p.getName().equals(name)) {
                player = p;
            }
        }
        return player;
    }

    /**
     * Check if coordinate contains a player
     *
     * @param x The x coordinate
     * @param y The y coordinate
     */
    public static Player getPlayerAt(int x, int y) {

        for (Player p : players) {
            if (p.getXpos() == x && p.getYpos() == y) {
                return p;
            }
        }
        return null;
    }

    /**
     * Move a player
     *
     * @param delta_x The number on the players x value to move
     * @param delta_y The number on the players y value to move
     * @param direction The direction of the move
     * @param player The player that wants moved
     * @return True if move was succesful
     */
    public static boolean playerMoved(int delta_x, int delta_y, Direction direction,
        Player player) {

        boolean moved = false;
        // dont move while in the middle of shooting
        if (!player.isShooting()) {
            // update player direction
            player.setDirection(direction);
            int x = player.getXpos(), y = player.getYpos();
            // check if the coordinates player wants to move to is a wall
            if (board[y + delta_y].charAt(x + delta_x) != 'w') {
                // check if the coordinates player wants to move to is occupied by another player
                Player p = getPlayerAt(x + delta_x, y + delta_y);
                if (p == null) {
                    moved = true;
                    x += delta_x;
                    y += delta_y;
                    player.setXpos(x);
                    player.setYpos(y);
                }
            }
        }
        return moved;
    }

    /**
     * Shoot with a player
     *
     * @param name Name of the player
     */
    public static void playerShoot(String name) {

        // find the player
        Player shooter = findPlayer(name);
        // check if player has enough ammo
        if (shooter.getAmmo() > 0) {
            int delta_x, delta_y;
            int x = shooter.getXpos();
            int y = shooter.getYpos();
            Direction direction = shooter.getDirection();
            if (direction == Direction.UP) {
                delta_x = 0;
                delta_y = -1;
            }
            else if (direction == Direction.DOWN) {
                delta_x = 0;
                delta_y = 1;
            }
            else if (direction == Direction.LEFT) {
                delta_x = -1;
                delta_y = 0;
            }
            else {
                delta_x = 1;
                delta_y = 0;
            }
            // save the first coordinate of the shot's path for later
            int startX = x + delta_x;
            int startY = y + delta_y;
            playerShot = null;
            // keep shooting until hitting a wall
            while (board[y + delta_y].charAt(x + delta_x) != 'w') {
                x += delta_x;
                y += delta_y;
                // check if a player is shot in this coordinate
                Player p = getPlayerAt(x, y);
                if (p != null) {
                    playerShot = p;
                    shooter.addPoints(50);
                    playerShot.addPoints(-50);
                    clientsScore();
                }
            }
            // if x and y has changed since we started shooting, it means we didnt hit a wall
            if (x != shooter.getXpos() || y != shooter.getYpos()) {
                // check if a player was shot
                if (playerShot != null) {
                    // save the location he was shot
                    int fromX = playerShot.getXpos();
                    int fromY = playerShot.getYpos();
                    // respawn the victim
                    spawn(playerShot);
                    // update threads a player is shooting
                    clientsShot(startX, startY, x, y, direction, name + "#" + fromX + "#" + fromY);
                    // update threads that victim was "moved"
                    clientsMovePlayer(fromX, fromY, playerShot.getXpos(), playerShot.getYpos(),
                        Direction.UP,
                        playerShot.getName());
                }
                // if no player was shot, just tell the threads that a player has shot
                else {
                    clientsShot(startX, startY, x, y, direction, name);
                }
                // adjust ammo after shot
                shooter.setAmmo(shooter.getAmmo() - 1);
            }
        }
    }
}
